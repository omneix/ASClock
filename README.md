# ASClock

## Description

Application for alarming about user-defined events.
Inspired by Free Alarm Clock (kudos to makers), which unfortunately don't have linux version.
Powered by Java.

![Main_UI](src/main/resources/images/ASClock_screens.png)
