package org.omneix.asclock;

import jaco.mp3.player.MP3Player;

import java.io.File;

public class WMP3Player extends MP3Player {

    public void stopAndClear() {
        this.stop();
        this.getPlayList().clear();
    }

    public void addToPlaylistAndPlay(String path) {
        this.addToPlayList(new File(path));
        this.play();
    }

}