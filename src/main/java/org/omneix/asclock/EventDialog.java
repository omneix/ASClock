package org.omneix.asclock;

import com.formdev.flatlaf.FlatClientProperties;
import com.github.lgooddatepicker.components.TimePicker;
import com.github.lgooddatepicker.components.TimePickerSettings;

import javax.swing.*;
import javax.swing.filechooser.FileNameExtensionFilter;
import java.awt.*;
import java.awt.event.*;
import java.io.File;
import java.text.DateFormat;
import java.time.LocalTime;
import java.util.*;

public class EventDialog extends JDialog {

    private final MainFrame mainFrame;
    private static final int DEFAULT_WIDTH = 400;
    private static final int DEFAULT_HEIGHT = 550;
    private final WMP3Player mp3Player;
    private final Event event;
    private final Operation op;
    private final ImageIcon STOP_ICON = new ImageIcon(Toolkit.getDefaultToolkit().getImage(getClass()
            .getResource("/images/stop.png")).getScaledInstance(10, 10, java.awt.Image.SCALE_SMOOTH));
    private final ImageIcon PLAY_ICON = new ImageIcon(Toolkit.getDefaultToolkit().getImage(getClass()
            .getResource("/images/play.png")).getScaledInstance(10, 10, java.awt.Image.SCALE_SMOOTH));

    public EventDialog(MainFrameView parent, Event event, Operation op, boolean modal) {
        super(parent, modal);
        this.mainFrame = parent.getMainFrame();
        this.event = event;
        this.op = op;
        this.mp3Player = mainFrame.getMp3Player();

        MiscTools.GUIRunAndWait(() -> {
            initComponents();
            setSize(DEFAULT_WIDTH, DEFAULT_HEIGHT);
            setTitle("Event - " + DateFormat.getDateTimeInstance(DateFormat.SHORT, 3).format(new Date()));
            setLocationRelativeTo(null);
            setVisible(true);
            setDefaultCloseOperation(WindowConstants.DISPOSE_ON_CLOSE);
            getRootPane().putClientProperty(FlatClientProperties.MENU_BAR_EMBEDDED, false);
            setResizable(false);
            setAlwaysOnTop(true);
        });
    }

    private void initComponents() {
        timeL = new JLabel("Time");
        timeSettings = new TimePickerSettings();
        timeSettings.setDisplayToggleTimeMenuButton(true);
        timeSettings.setDisplaySpinnerButtons(true);
        timeSettings.setInitialTimeToNow();
        // HH will be in 24h format
        timeSettings.setFormatForDisplayTime("hh:mm a");
        timePicker = new TimePicker(timeSettings);
        timePicker.setMaximumSize(timePicker.getPreferredSize());

        // Changing timepicker style when dark theme is set, default is not matching with whole style
        if (PrefManager.getTheme() == 2) {
            timeSettings.setColor(TimePickerSettings.TimeArea.TimePickerTextValidTime, Color.lightGray);
            timeSettings.setColor(TimePickerSettings.TimeArea.TextFieldBackgroundValidTime, new Color(timeL.getBackground().getRGB()));
        }

        repeatL = new JLabel("Repeat");
        chBox0 = new JCheckBox("Everyday");
        chBox0.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent evt) {
                chBox0ActionPerformed(evt);
            }
        });

        chBox1 = new JCheckBox("Every Monday");
        chBox2 = new JCheckBox("Every Tuesday");
        chBox3 = new JCheckBox("Every Wednesday");
        chBox4 = new JCheckBox("Every Thursday");
        chBox5 = new JCheckBox("Every Friday");
        chBox6 = new JCheckBox("Every Saturday");
        chBox7 = new JCheckBox("Every Sunday");

        for (JCheckBox c : new JCheckBox[]{chBox1, chBox2, chBox3, chBox4, chBox5, chBox6, chBox7}) {
            c.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                    setChBox0Selection(c);
                }
            });
        }

        messageL = new JLabel("Message:");
        messageA = new JTextArea("Alarm");
        chBox8 = new JCheckBox("Show message");
        chBox8.setSelected(true);

        soundsCB = new JComboBox<>(mainFrame.getSoundList());
        soundsCB.setMinimumSize(new Dimension(255, 21));
        soundsCB.setMaximumSize(new Dimension(255, 21));
        soundsCB.setSize(new Dimension(255, 21));
        if (mainFrame.getSoundList().length > 7) {
            soundsCB.setSelectedIndex(6);
        } else {
            soundsCB.setSelectedIndex(0);
        }
        soundsCB.setEditable(true);

        messageAScrollP = new JScrollPane();
        messageAScrollP.setViewportView(messageA);
        messageAScrollP.setMaximumSize(new Dimension(365, 80));

        soundL = new JLabel("Sound (File.mp3):");

        playB = new JButton();
        playB.setMinimumSize(new Dimension(22, 22));
        playB.setIcon(new ImageIcon(Toolkit.getDefaultToolkit().getImage(getClass().getResource("/images/play.png")).getScaledInstance(10, 10, java.awt.Image.SCALE_SMOOTH)));
        playB.addActionListener(this::playBActionPerformed);

        fileChooser = new JFileChooser();
        fileChooser.setFileFilter(new FileNameExtensionFilter(".mp3", "mp3"));
        fileChooser.setAcceptAllFileFilterUsed(false);

        browseB = new JButton("Browse...");
        browseB.setMinimumSize(new Dimension(23, 23));
        browseB.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent evt) {
                openFileChooser(evt);
            }
        });

        okB = new JButton("OK");
        okB.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent evt) {
                okBActionPerformed(evt);
            }
        });

        cancelB = new JButton("Cancel");
        cancelB.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent evt) {
                //TODO do wrapper to mp3player for auto clearing after stop!!!
                mp3Player.stop();
                mp3Player.getPlayList().clear();
                dispose();
            }
        });

        // Incoming event state restore
        if (event != null) {
            timePicker.setTime(LocalTime.parse(event.getTime()));
            chBox0.setSelected(event.getScheduledDays()[0]);
            chBox1.setSelected(event.getScheduledDays()[1]);
            chBox2.setSelected(event.getScheduledDays()[2]);
            chBox3.setSelected(event.getScheduledDays()[3]);
            chBox4.setSelected(event.getScheduledDays()[4]);
            chBox5.setSelected(event.getScheduledDays()[5]);
            chBox6.setSelected(event.getScheduledDays()[6]);
            chBox7.setSelected(event.getScheduledDays()[7]);
            chBox8.setSelected(event.isCb8Check());
            messageA.setText(event.getMessageAText());
            soundsCB.setSelectedItem(event.getSoundsCBText());
        }

        soundsCB.addItemListener(new ItemListener() {
            @Override
            public void itemStateChanged(ItemEvent evt) {
                soundsCBItemChanged(evt);
            }
        });

        this.addWindowListener(new WindowAdapter() {
            @Override
            public void windowClosing(WindowEvent evt) {
                mp3Player.stop();
                mp3Player.getPlayList().clear();
                super.windowClosing(evt);
            }
        });

        //Another monstrous layout
        GroupLayout layout = new GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(layout.createSequentialGroup()
                .addGap(10)
                .addGroup(layout.createParallelGroup(GroupLayout.Alignment.LEADING)
                        .addComponent(timeL)
                        .addComponent(timePicker)
                        .addComponent(repeatL)
                        .addComponent(chBox0)
                        .addComponent(chBox1)
                        .addComponent(chBox2)
                        .addComponent(chBox3)
                        .addComponent(chBox4)
                        .addComponent(chBox5)
                        .addComponent(chBox6)
                        .addComponent(chBox7)
                        .addComponent(messageL)
                        .addComponent(messageAScrollP)
                        .addComponent(chBox8)
                        .addComponent(soundL).addGap(3)
                        .addGroup(layout.createSequentialGroup()
                                .addComponent(soundsCB)
                                .addGap(5)
                                .addComponent(playB)
                                .addGap(5)
                                .addComponent(browseB))
                        .addGroup(layout.createSequentialGroup()
                                .addContainerGap(215, 300)
                                .addComponent(okB).addGap(5)
                                .addComponent(cancelB)))
                .addGap(10));

        layout.setVerticalGroup(layout.createSequentialGroup().addGap(3)
                .addComponent(timeL).addGap(3)
                .addComponent(timePicker).addGap(15)
                .addComponent(repeatL).addGap(3)
                .addComponent(chBox0).addGap(3)
                .addComponent(chBox1).addGap(3)
                .addComponent(chBox2).addGap(3)
                .addComponent(chBox3).addGap(3)
                .addComponent(chBox4).addGap(3)
                .addComponent(chBox5).addGap(3)
                .addComponent(chBox6).addGap(3)
                .addComponent(chBox7).addGap(15)
                .addComponent(messageL).addGap(3)
                .addComponent(messageAScrollP).addGap(3)
                .addComponent(chBox8).addGap(15)
                .addComponent(soundL).addGap(3)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(soundsCB).addGap(2)
                        .addComponent(playB)
                        .addComponent(browseB)).addContainerGap(23, 50)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(okB)
                        .addComponent(cancelB)).addGap(10));
        pack();
    }

    private void okBActionPerformed(ActionEvent evt) {
        Event event = new Event();
        int count = 0;
        for (JCheckBox c : new JCheckBox[]{chBox0, chBox1, chBox2, chBox3, chBox4, chBox5, chBox6, chBox7}) {
            if (c.isSelected()) {
                event.getScheduledDays()[count] = true;
            }
            count++;
        }
        event.setCb8Check(chBox8.isSelected());
        event.setMessageAText(messageA.getText());
        event.setSoundsCBText(Objects.requireNonNull(soundsCB.getSelectedItem()).toString());
        event.setTime(timePicker.getTime().toString());
        event.setEnabled(true);
        if (op == Operation.ADD) {
            mainFrame.getEventList().add(event);
        } else if (op == Operation.MODIFY) {
            mainFrame.getEventList().set(this.event.getNumber(), event);
        }
        int selectedRow = mainFrame.getView().getTableSelectedRow();
        mainFrame.performRearrange();
        if (selectedRow != -1) {
            mainFrame.getView().setTableSelectedRow(selectedRow);
        }
        if (!mp3Player.isStopped()) mp3Player.stopAndClear();
        this.dispose();
    }

    private void openFileChooser(ActionEvent evt) {
        int userSelection = fileChooser.showOpenDialog(this);
        if (userSelection == JFileChooser.APPROVE_OPTION) {
            File file = fileChooser.getSelectedFile();
            soundsCB.setSelectedItem(file.getAbsolutePath());
        }
    }

    private void setChBox0Selection(JCheckBox c) {
        if (!c.isSelected()) {
            chBox0.setSelected(false);
        }
        if (chBox1.isSelected() && chBox2.isSelected() && chBox3.isSelected() && chBox4.isSelected() &&
                chBox5.isSelected() && chBox6.isSelected() && chBox7.isSelected()) {
            chBox0.setSelected(true);
        }
    }

    private void chBox0ActionPerformed(ActionEvent evt) {
        MiscTools.GUIRun(() -> {
            if (chBox0.isSelected()) {
                for (JCheckBox c : new JCheckBox[]{chBox1, chBox2, chBox3, chBox4, chBox5, chBox6, chBox7}) {
                    c.setSelected(true);
                }
            } else {
                for (JCheckBox c : new JCheckBox[]{chBox1, chBox2, chBox3, chBox4, chBox5, chBox6, chBox7}) {
                    c.setSelected(false);
                }
            }
        });
    }

    //TODO change play icon to STOP when song naturally ends
    private void playBActionPerformed(ActionEvent evt) {
        String sndPath = getFullSoundPath();
        if (mp3Player.isStopped() && sndPath != null) {
            mp3Player.addToPlaylistAndPlay(sndPath);
            playB.setIcon(STOP_ICON);
        } else {
            mp3Player.stopAndClear();
            playB.setIcon(PLAY_ICON);
        }
    }

    private void soundsCBItemChanged(ItemEvent evt) {
        if (evt.getStateChange() == ItemEvent.SELECTED) {
            String fullSoundPath = getFullSoundPath();
            if (fullSoundPath == null) {
                playB.setIcon(PLAY_ICON);
                mp3Player.stopAndClear();
                return;
            }
            mp3Player.stopAndClear();
            mp3Player.addToPlaylistAndPlay(fullSoundPath);
            playB.setIcon(STOP_ICON);
        }
    }

    private String getFullSoundPath() {
        String fullSoundPath;
        if (soundsCB.getSelectedIndex() == -1) {
            fullSoundPath = (String) soundsCB.getSelectedItem();
        } else if (soundsCB.getSelectedIndex() == 0 || mainFrame.getSoundDirPath() == null) {
            return null;
        } else {
            fullSoundPath = mainFrame.getSoundDirPath() + "\\" + soundsCB.getSelectedItem() + ".mp3";
        }
        return fullSoundPath;
    }

    private JLabel timeL;
    private TimePickerSettings timeSettings;
    private TimePicker timePicker;
    private JLabel repeatL;
    private JCheckBox chBox0;
    private JCheckBox chBox1;
    private JCheckBox chBox2;
    private JCheckBox chBox3;
    private JCheckBox chBox4;
    private JCheckBox chBox5;
    private JCheckBox chBox6;
    private JCheckBox chBox7;
    private JLabel messageL;
    private JTextArea messageA;
    private JScrollPane messageAScrollP;
    private JCheckBox chBox8;
    private JLabel soundL;
    private JComboBox<String> soundsCB;
    private JButton playB;
    private JButton browseB;
    private JFileChooser fileChooser;
    private JButton okB;
    private JButton cancelB;
}

enum Operation {
    ADD, MODIFY;
}