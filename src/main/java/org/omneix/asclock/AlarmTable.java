package org.omneix.asclock;

import javax.swing.*;
import javax.swing.border.Border;
import javax.swing.border.CompoundBorder;
import javax.swing.border.EmptyBorder;
import javax.swing.border.MatteBorder;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import javax.swing.table.DefaultTableCellRenderer;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableCellRenderer;
import java.awt.*;
import java.time.LocalTime;
import java.time.format.DateTimeFormatter;
import java.util.*;
import java.util.List;
import java.util.stream.IntStream;

public class AlarmTable extends JPanel {

    private JTable table;
    private JScrollPane jScrollPane;
    private TableModel tableModel;
    private ListSelectionModel lsm;

    public ListSelectionModel getLsm() {
        return lsm;
    }

    public JTable getTable() {
        return table;
    }

    public AlarmTable() {
        setLayout(new BoxLayout(this, BoxLayout.Y_AXIS));

        tableModel = new TableModel();
        table = new JTable(tableModel) {
            private Border outside = new MatteBorder(0, 0, 1, 0, Color.LIGHT_GRAY);
            private Border inside = new EmptyBorder(1, 1, 0, 1);
            private Border highlight = new CompoundBorder(outside, inside);

            public Component prepareRenderer(
                    TableCellRenderer renderer, int row, int column) {
                Component c = super.prepareRenderer(renderer, row, column);
                JComponent jc = (JComponent) c;

                // Add a border to the selected row
                jc.setBorder(highlight);

                return c;
            }
        };

        table.setTableHeader(null);
        table.setPreferredScrollableViewportSize(new Dimension(373, 450));
        table.setRowHeight(30);
        table.setShowGrid(false);
        table.setFillsViewportHeight(true);
        table.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
        lsm = this.table.getSelectionModel();

        table.getColumnModel().getColumn(0).setMinWidth(35);
        table.getColumnModel().getColumn(0).setMaxWidth(35);
        table.getColumnModel().getColumn(1).setMinWidth(60);
        table.getColumnModel().getColumn(1).setMaxWidth(60);
        table.getColumnModel().getColumn(2).setMinWidth(100);
        table.getColumnModel().getColumn(2).setPreferredWidth(200);
        table.getColumnModel().getColumn(2).setMaxWidth(200);

        jScrollPane = new JScrollPane(table);
        add(jScrollPane);

        table.setDefaultRenderer(String.class, new CustomRenderer());
    }

    public TableModel getTableModel() {
        return tableModel;
    }
}

class TableModel extends DefaultTableModel {

    private final String[] columnNames = {
            "Enabled",
            "Time",
            "Days",
            "Message",
    };

    public void addRow(Event event) {
        if (event == null) {
            throw new IllegalArgumentException("rowData cannot be null");
        }
        Vector<Object> rowVector = new Vector<>();
        rowVector.add(event.isEnabled());
        rowVector.add(LocalTime.parse(event.getTime())
                .format(DateTimeFormatter.ofPattern("hh:mm a"))
                .toUpperCase());
        rowVector.add(getDays(event.getScheduledDays()));
        if (event.isCb8Check()) {
            rowVector.add(event.getMessageAText());
        }
        super.addRow(rowVector);
    }

    public void changeRow(int rowNumber, Event event) {
        if (event == null) {
            throw new IllegalArgumentException("rowData cannot be null");
        }
        super.setValueAt(event.isEnabled(), rowNumber,0);
        super.setValueAt(LocalTime.parse(event.getTime())
                .format(DateTimeFormatter.ofPattern("hh:mm a"))
                .toUpperCase(),
                rowNumber,1);
        super.setValueAt(getDays(event.getScheduledDays()), rowNumber, 2);
        if (event.isCb8Check()) {
            super.setValueAt(event.getMessageAText(), rowNumber, 3);
        } else {
            super.setValueAt("", rowNumber, 3);
        }
    }

    private String getDays(boolean[] scheduledDays) {
        StringBuilder sb = new StringBuilder();
        if (scheduledDays[0]) {
            return "Everyday";
        }
        if (scheduledDays[1]) {
            sb.append("Mon, ");
        }
        if (scheduledDays[2]) {
            sb.append("Tue, ");
        }
        if (scheduledDays[3]) {
            sb.append("Wed, ");
        }
        if (scheduledDays[4]) {
            sb.append("Thu, ");
        }
        if (scheduledDays[5]) {
            sb.append("Fri, ");
        }
        if (scheduledDays[6]) {
            sb.append("Sat, ");
        }
        if (scheduledDays[7]) {
            sb.append("Sun, ");
        }
        if (sb.toString().equals("")) return sb.toString();
        return sb.substring(0, sb.length() - 2);
    }

    public void refillTableFromList(List<Event> eventList) {
        super.getDataVector().clear();
        IntStream.range(0,eventList.size()).forEach(i -> addRow(eventList.get(i)));
    }

    public int getColumnCount() {
        return columnNames.length;
    }

    public String getColumnName(int col) {
        return columnNames[col];
    }

    /*
     * JTable uses this method to determine the default renderer/
     * editor for each cell.  If we didn't implement this method,
     * then the last column would contain text ("true"/"false"),
     * rather than a check box.
     */
    public Class<?> getColumnClass(int columnIndex) {
        if (getRowCount() > 0 && getValueAt(0, columnIndex) != null) {
            return getValueAt(0, columnIndex).getClass();
        }
        return super.getColumnClass(columnIndex);
    }

    public boolean isCellEditable(int row, int col) {
        if (col == 0) {
            return true;
        } else {
            return false;
        }
    }

    @Override
    public void setValueAt(Object aValue, int row, int column) {
        super.setValueAt(aValue, row, column);
    }
}

class CustomRenderer extends DefaultTableCellRenderer {
    @Override
    public Component getTableCellRendererComponent(
            JTable table, Object value, boolean isSelected, boolean hasFocus, int row, int column) {

        super.getTableCellRendererComponent(table, value, isSelected, hasFocus, row, column);
        if (column == 1 || column == 2) {
            setFont(this.getFont().deriveFont(Font.BOLD));
        }
        setHorizontalAlignment( JLabel.CENTER );
        return this;
    }

}