package org.omneix.asclock;

import com.formdev.flatlaf.FlatDarkLaf;
import com.formdev.flatlaf.FlatLightLaf;

import javax.swing.*;
import java.awt.*;

public class MiscTools {

    public static void setLookAndFeel(int theme) {
        LookAndFeel LaF = new FlatLightLaf();
        switch (theme) {
            case 1:
                LaF = new FlatLightLaf();
                break;
            case 2:
                LaF = new FlatDarkLaf();
                break;
        }
        try {
            UIManager.setLookAndFeel(LaF);
        } catch (Exception ex) {
            System.err.println("Failed to initialize LaF");
        }
    }

    public static void setFont(JFrame frame) {
        Font font;
        String currentOs = System.getProperty("os.name").toLowerCase();
        if (currentOs.contains("win")) {
            font = new javax.swing.plaf.FontUIResource("SEGOE UI", Font.PLAIN, 12);
        } else {
            font = new javax.swing.plaf.FontUIResource("Arial", Font.PLAIN, 12);
        }
        java.util.Enumeration keys = UIManager.getDefaults().keys();
        while (keys.hasMoreElements()) {
            Object key = keys.nextElement();
            Object value = UIManager.get(key);
            if (value instanceof javax.swing.plaf.FontUIResource) UIManager.put(key, font);
        }
    }

    public static void GUIRunAndWait(Runnable r) {
        boolean ok;
        do {
            ok = true;
            try {
                if (!SwingUtilities.isEventDispatchThread()) {
                    SwingUtilities.invokeAndWait(r);
                } else {
                    r.run();
                }
            } catch (Exception ex) {
                ok = false;
            }
        } while (!ok);
    }

    public static void GUIRun(Runnable r) {
        boolean ok;
        do {
            ok = true;
            try {
                if (!SwingUtilities.isEventDispatchThread()) {
                    SwingUtilities.invokeLater(r);
                } else {
                    r.run();
                }
            } catch (Exception ex) {
                ok = false;
            }
        } while (!ok);
    }

}
