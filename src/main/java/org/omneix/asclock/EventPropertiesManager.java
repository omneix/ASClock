package org.omneix.asclock;

import com.fasterxml.jackson.core.util.DefaultPrettyPrinter;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.ObjectWriter;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class EventPropertiesManager {

    private static final File defaultFile = new File(System.getProperty("user.home") + "/.config/events.arm");
    private static final ObjectWriter writer = new ObjectMapper().writer(new DefaultPrettyPrinter());
    private static final ObjectMapper mapper = new ObjectMapper();

    public static void saveEventsToFile(List<Event> list) {
        if (!defaultFile.exists()) {
            defaultFile.getParentFile().mkdirs();
        }
        try {
            writer.writeValue(defaultFile, list);
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    public static void saveEventsToFile(List<Event> list, File file) {
        if (!file.getName().matches(".*\\.arm")) {
            file = new File(file.getAbsolutePath() + ".arm");
        }
        try {
            writer.writeValue(file, list);
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    // Quirks with object types, Arrays.asList() gives immutable collection
    // Do not assign without constructor
    public static List<Event> loadEventsFromFile() {
        List<Event> eventList;
        if (!defaultFile.exists()) { return new ArrayList<>(); }
        try {
            eventList = new ArrayList<>(Arrays.asList(mapper.readValue(defaultFile, Event[].class)));
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
        return eventList;
    }

    public static List<Event> loadEventsFromFile(File file) {
        List<Event> eventList;
        try {
            eventList = new ArrayList<>(Arrays.asList(mapper.readValue(file, Event[].class)));
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
        return eventList;
    }

}
