package org.omneix.asclock;

import java.util.prefs.Preferences;

/**
 * Wrapper around preferences.
 * Loads them at start, saves on exit.
 */

public class PrefManager {

    private static final Preferences root = Preferences.userRoot();
    private static final Preferences node = root.node("/asclock");
    private static boolean onTop;
    private static int theme;
    private static boolean trayIcon;

    public static void loadPreferences() {
        onTop = node.getBoolean("on_top", false);
        theme = node.getInt("theme", 1);
        trayIcon = node.getBoolean("tray_icon", true);
    }

    public static void setOnTop(boolean onTop) {
        PrefManager.onTop = onTop;
    }

    public static void setTheme(int theme) {
        PrefManager.theme = theme;
    }

    public static void setTrayIcon(boolean trayIcon) {
        PrefManager.trayIcon = trayIcon;
    }

    public static void savePreferences() {
        node.putBoolean("on_top", onTop);
        node.putInt("theme", theme);
        node.putBoolean("tray_icon", trayIcon);
    }

    public static boolean isOnTop() {
        return onTop;
    }

    public static int getTheme() {
        return theme;
    }

    public static boolean isTrayIcon() {
        return trayIcon;
    }


}
