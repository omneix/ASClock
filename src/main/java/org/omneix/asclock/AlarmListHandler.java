package org.omneix.asclock;

import java.time.LocalDate;
import java.time.LocalTime;
import java.util.HashMap;
import java.util.List;

public class AlarmListHandler {

    public static void getAllAlarms(List<Event> eventList, HashMap<LocalTime, Integer> alarmMap) {
        alarmMap.clear();
        for (Event event : eventList) {
            getAlarmsForEvent(event, alarmMap);
        }
    }

    public static void getAlarmsForEvent(Event event, HashMap<LocalTime, Integer> alarmMap) {
        int day = LocalDate.now().getDayOfWeek().getValue();
        if (event.isEnabled() && (event.isEveryday() || event.getScheduledDays()[day])) {
            alarmMap.put(LocalTime.parse(event.getTime()), event.getNumber());
        }
    }

    public static void deleteAlarmsForEvent(Event event, HashMap<LocalTime, Integer> alarmMap) {
        alarmMap.values().remove(event.getNumber());
    }

}
