package org.omneix.asclock;

import com.formdev.flatlaf.FlatClientProperties;

import javax.swing.*;
import javax.swing.event.TableModelEvent;
import javax.swing.filechooser.FileNameExtensionFilter;
import java.awt.*;
import java.awt.event.*;
import java.io.File;
import java.text.DateFormat;
import java.util.Date;
import java.util.Objects;

import static javax.swing.KeyStroke.getKeyStroke;

public class MainFrameView extends JFrame {

    private static final int DEFAULT_WIDTH = 400;
    private static final int DEFAULT_HEIGHT = 550;
    private final MainFrame mainFrame;
    private AlarmTable alarmTable;

    public MainFrameView(MainFrame mainFrame) {
        this.mainFrame = mainFrame;

        MiscTools.GUIRunAndWait(() -> {
            setSize(DEFAULT_WIDTH, DEFAULT_HEIGHT);
            MiscTools.setFont(this);
            setAlwaysOnTop(PrefManager.isOnTop());
            initComponents();
            setDateOnTitle();
            setDefaultCloseOperation(WindowConstants.HIDE_ON_CLOSE);
            setLocation(1450, 300);
            setVisible(true);
            setIconImage(Toolkit.getDefaultToolkit().getImage(getClass().getResource("/images/clock.png")));
            getRootPane().putClientProperty(FlatClientProperties.MENU_BAR_EMBEDDED, false);
        });
    }

    public void setDateOnTitle() {
        Timer t = new Timer(500, e -> setTitle(
                DateFormat.getDateTimeInstance(DateFormat.FULL, 2).format(new Date()) + " - ASClock"));
        t.setRepeats(true);
        t.setCoalesce(true);
        t.setInitialDelay(0);
        t.start();
    }

    public void initComponents() {
        fileChooser = new JFileChooser();
        FileNameExtensionFilter filter = new FileNameExtensionFilter(".arm", "arm");
        fileChooser.setFileFilter(filter);
        fileChooser.setAcceptAllFileFilterUsed(false);

        // Menu Bar
        menuBar = new JMenuBar();
        sep = new JSeparator();
        // File Menu
        fileMenu = new JMenu("File");
        loadM = new JMenuItem("Load from File");
        loadM.setIcon(new ImageIcon(Objects.requireNonNull(getClass().getResource("/images/load.png"))));
        loadM.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent evt) {
                loadEventsActionPerformed(evt);
            }
        });

        saveM = new JMenuItem("Save to File");
        saveM.setIcon(new ImageIcon(Objects.requireNonNull(getClass().getResource("/images/save.png"))));
        saveM.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent evt) {
                saveEventsActionPerformed(evt);
            }
        });

        exitM = new JMenuItem("Exit");
        exitM.setIcon(new ImageIcon(Objects.requireNonNull(getClass().getResource("/images/exit.png"))));
        exitM.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent evt) {
                exitMenuActionPerformed();
            }
        });

        fileMenu.add(loadM);
        fileMenu.add(saveM);
        fileMenu.add(sep);
        fileMenu.add(exitM);
        menuBar.add(fileMenu);

        // Actions menu
        actionsMenu = new JMenu("Actions");
        addM = new JMenuItem("Add");
        addM.setIcon(new ImageIcon(Objects.requireNonNull(getClass().getResource("/images/add.png"))));
        addM.setAccelerator(getKeyStroke(KeyEvent.VK_N, Toolkit.getDefaultToolkit().getMenuShortcutKeyMask()));
        addM.setMnemonic('N');
        addM.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent evt) {
                addEventActionPerformed(evt);
            }
        });

        deleteM = new JMenuItem("Delete");
        deleteM.setIcon(new ImageIcon(Objects.requireNonNull(getClass().getResource("/images/delete.png"))));
        deleteM.setAccelerator(getKeyStroke(KeyEvent.VK_D, Toolkit.getDefaultToolkit().getMenuShortcutKeyMask()));
        deleteM.setMnemonic('D');
        deleteM.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent evt) {
                deleteEventActionPerformed(evt);
            }
        });

        editM = new JMenuItem("Edit");
        editM.setIcon(new ImageIcon(Objects.requireNonNull(getClass().getResource("/images/edit.png"))));
        editM.setAccelerator(getKeyStroke(KeyEvent.VK_E, Toolkit.getDefaultToolkit().getMenuShortcutKeyMask()));
        editM.setMnemonic('E');
        editM.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent evt) {
                editEventActionPerformed(evt);
            }
        });

        copyM = new JMenuItem("Copy");
        copyM.setIcon(new ImageIcon(Objects.requireNonNull(getClass().getResource("/images/copy.png"))));

        actionsMenu.add(addM);
        actionsMenu.add(deleteM);
        actionsMenu.add(editM);
        actionsMenu.add(copyM);
        menuBar.add(actionsMenu);

        // Options menu
        optionsMenu = new JMenu("Options");

        stayOnTopM = new JCheckBoxMenuItem("Stay On Top");
        stayOnTopM.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent evt) {
                stayOnTopMActionPerformed();
            }
        });
        trayM = new JCheckBoxMenuItem("Tray icon");

        themeMenu = new JMenu("Theme");
        lightThemeM = new JMenuItem("FlatLight");
        lightThemeM.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                changeThemeActionPerformed(1);
            }
        });
        darkThemeM = new JMenuItem("FlatDark");
        darkThemeM.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                changeThemeActionPerformed(2);
            }
        });
        themeMenu.add(lightThemeM);
        themeMenu.add(darkThemeM);

        optionsMenu.add(trayM);
        optionsMenu.add(stayOnTopM);
        optionsMenu.add(themeMenu);
        menuBar.add(optionsMenu);

        // About menu
        aboutMenu = new JMenu("About");
        aboutM = new JMenuItem("ASClock");
        aboutM.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                new AboutDialog(MainFrameView.this, false);
            }
        });
        aboutMenu.add(aboutM);
        menuBar.add(aboutMenu);

        setJMenuBar(menuBar);

        // Alarm panel properties
        alarmTable = new AlarmTable();
        // Reset of action for Enter key
        alarmTable.getTable().getInputMap(JComponent.WHEN_ANCESTOR_OF_FOCUSED_COMPONENT)
                .put(KeyStroke.getKeyStroke("ENTER"), "none");
        alarmTable.getTableModel().refillTableFromList(mainFrame.getEventList());
        alarmTable.getTable().addKeyListener(new KeyAdapter() {
            @Override
            public void keyPressed(KeyEvent evt) {
                alarmTableKeyAction(evt);
            }
        });
        alarmTable.getTable().addMouseListener(new MouseAdapter() {
            public void mouseClicked(MouseEvent evt) {
                alarmTableMouseAction(evt);
            }
        });
        alarmTable.getTableModel().addTableModelListener(this::alarmTableListener);

        addB = new JButton("Add");
        setDimensionsAndUnfocus(addB);
        addB.setIcon(new ImageIcon(Objects.requireNonNull(getClass().getResource("/images/add.png"))));
        addB.setToolTipText("Add new alarm (Ctrl+N)");
        addB.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent evt) {
                addEventActionPerformed(evt);
            }
        });

        // Edit Button properties
        editB = new JButton("Edit");
        setDimensionsAndUnfocus(editB);
        editB.setIcon(new ImageIcon(Objects.requireNonNull(getClass().getResource("/images/edit.png"))));
        editB.setToolTipText("Edit alarm (Ctrl+E)");
        editB.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent evt) {
                editEventActionPerformed(evt);
            }
        });

        // Delete Button properties
        deleteB = new JButton("Delete");
        setDimensionsAndUnfocus(deleteB);
        deleteB.setIcon(new ImageIcon(Objects.requireNonNull(getClass().getResource("/images/delete.png"))));
        deleteB.setToolTipText("Delete alarm (Ctrl+D)");
        deleteB.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent evt) {
                deleteEventActionPerformed(evt);
            }
        });

        // Copy Button properties
        copyB = new JButton("Copy");
        setDimensionsAndUnfocus(copyB);
        copyB.setIcon(new ImageIcon(Objects.requireNonNull(getClass().getResource("/images/copy.png"))));
        copyB.setToolTipText("Copy alarm");
        copyB.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent evt) {
                copyEventActionPerformed(evt);
            }
        });

        // Tray section
        popupM = new PopupMenu();
        trayIcon = new TrayIcon(Toolkit.getDefaultToolkit().getImage(getClass().getResource("/images/clock.png")));
        trayIcon.setImageAutoSize(true);
        trayIcon.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent evt) {
                if (evt.getButton() == MouseEvent.BUTTON1) {
                    if (MainFrameView.super.isVisible()) {
                        MainFrameView.super.setVisible(false);
                    } else {
                        setViewVisibleAndFocus();
                    }
                }
            }
        });

        // Tray menu
        mainT = new MenuItem("ASClock");
        mainT.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent evt) {
                setViewVisibleAndFocus();
            }
        });

        addEventT = new MenuItem("Add");
        addEventT.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent evt) {
                setViewVisibleAndFocus();
                addEventActionPerformed(evt);
            }
        });

        aboutT = new MenuItem("About ASClock");
        aboutT.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent evt) {
                new AboutDialog(MainFrameView.this, false);
            }
        });

        exitT = new MenuItem("Exit");
        exitT.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent evt) {
                exitMenuActionPerformed();
            }
        });

        popupM.add(mainT);
        popupM.addSeparator();
        popupM.add(addEventT);
        popupM.addSeparator();
        popupM.add(aboutT);
        popupM.addSeparator();
        popupM.add(exitT);

        trayIcon.setPopupMenu(popupM);

        if (!SystemTray.isSupported()) {
            System.out.println("SystemTray is not supported");
            return;
        }
        tray = SystemTray.getSystemTray();
        if (PrefManager.isTrayIcon()) {
            try {
                tray.add(trayIcon);
                trayM.setSelected(true);
            } catch (AWTException e) {
                System.out.println("TrayIcon could not be added.");
            }
        }

        trayM.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent evt) {
                addTrayMActionPerformed(evt);
            }
        });

        addWindowStateListener(new WindowStateListener() {
            public void windowStateChanged(WindowEvent evt) {
                if (evt.getNewState() == ICONIFIED && PrefManager.isTrayIcon()) {
                    setVisible(false);
                }
            }
        });

        addWindowListener(new WindowAdapter() {
            public void windowClosing(WindowEvent evt) {
                if (!PrefManager.isTrayIcon()) {
                    exitMenuActionPerformed();
                }
            }
        });

        // Behold, The Layout
        GroupLayout layout = new GroupLayout(getContentPane());
        getContentPane().setLayout(layout);

        layout.setHorizontalGroup(layout.createSequentialGroup()
                .addGap(5)
                .addGroup(layout.createParallelGroup(GroupLayout.Alignment.LEADING)
                        .addGroup(layout.createSequentialGroup()    //for equal button size insert lines below in corresponding .addComponent()
                                .addComponent(addB) //, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE
                                .addGap(5)
                                .addComponent(deleteB) //, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE
                                .addGap(5)
                                .addComponent(editB) //, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE
                                .addGap(5)
                                .addComponent(copyB)) //, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE
                                .addComponent(alarmTable))
                .addGap(5));

        layout.setVerticalGroup(layout.createSequentialGroup()
                .addGap(5)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(addB)
                        .addGap(3)
                        .addComponent(deleteB)
                        .addGap(3)
                        .addComponent(editB)
                        .addGap(3)
                        .addComponent(copyB))
                .addGap(5)
                .addComponent(alarmTable)
                .addGap(5));
        pack();
    }

    public MainFrame getMainFrame() {
        return mainFrame;
    }

    public AlarmTable getAlarmTable() {
        return alarmTable;
    }

    public int getTableSelectedRow() {
        return alarmTable.getTable().getSelectedRow();
    }

    public void setTableSelectedRow(int row) {
        alarmTable.getTable().setRowSelectionInterval(row, row);
    }

    private void alarmTableListener(TableModelEvent evt) {
        if (evt.getType() == TableModelEvent.UPDATE && evt.getColumn() == 0) {
            int selectedRow = evt.getFirstRow();
            boolean enabled = (boolean) alarmTable.getTableModel().getValueAt(selectedRow, 0);
            if (enabled) {
                mainFrame.getEventList().get(selectedRow).setEnabled(true);
                AlarmListHandler.getAlarmsForEvent(mainFrame.getEventList().get(selectedRow), mainFrame.getAlarmMap());
                return;
            }
            mainFrame.getEventList().get(selectedRow).setEnabled(false);
            AlarmListHandler.deleteAlarmsForEvent(mainFrame.getEventList().get(selectedRow), mainFrame.getAlarmMap());
        }
    }

    private void setViewVisibleAndFocus() {
        MainFrameView.super.setVisible(true);
        MainFrameView.super.toFront();
        MainFrameView.super.setState(Frame.NORMAL);
    }

    private void addTrayMActionPerformed(ActionEvent evt) {
        if (trayM.isSelected()) {
            try {
                tray.add(trayIcon);
            } catch (AWTException e) {
                System.out.println("TrayIcon could not be added.");
            }
            PrefManager.setTrayIcon(true);
        } else {
            tray.remove(trayIcon);
            PrefManager.setTrayIcon(false);
        }
    }

    private void stayOnTopMActionPerformed() {
        if (stayOnTopM.isSelected()) {
            mainFrame.getView().setAlwaysOnTop(true);
            PrefManager.setOnTop(true);
        } else {
            mainFrame.getView().setAlwaysOnTop(false);
            PrefManager.setOnTop(false);
        }
    }

    private void changeThemeActionPerformed(int themeNumber) {
        PrefManager.setTheme(themeNumber);
        MiscTools.setLookAndFeel(themeNumber);
        SwingUtilities.updateComponentTreeUI(mainFrame.getView());
    }

    private void exitMenuActionPerformed() {
        EventPropertiesManager.saveEventsToFile(mainFrame.getEventList());
        PrefManager.savePreferences();
        System.exit(0);
    }

    private void saveEventsActionPerformed(ActionEvent evt) {
        int userSelection = fileChooser.showSaveDialog(this);
        if (userSelection == JFileChooser.APPROVE_OPTION) {
            File file = fileChooser.getSelectedFile();
            EventPropertiesManager.saveEventsToFile(mainFrame.getEventList(), file);
        }
    }

    private void loadEventsActionPerformed(ActionEvent evt) {
        int userSelection = fileChooser.showOpenDialog(this);
        if (userSelection == JFileChooser.APPROVE_OPTION) {
            mainFrame.setEventList(EventPropertiesManager.loadEventsFromFile(fileChooser.getSelectedFile()));
            alarmTable.getTableModel().refillTableFromList(mainFrame.getEventList());
        }
    }

    private void alarmTableMouseAction(MouseEvent evt) {
        if (evt.getClickCount() == 2 && alarmTable.getTable().getSelectedColumn() > 0) {
            int row = alarmTable.getTable().getSelectedRow();
            new EventDialog(mainFrame.getView(), mainFrame.getEventList().get(row), Operation.MODIFY, true);
        }
    }

    private void alarmTableKeyAction(KeyEvent evt) {
        if (evt.getKeyCode() == KeyEvent.VK_ENTER) {
            int row = alarmTable.getTable().getSelectedRow();
            new EventDialog(mainFrame.getView(), mainFrame.getEventList().get(row), Operation.MODIFY, true);
        }
    }

    private void addEventActionPerformed(ActionEvent evt) {
        unArmButton(evt);
        new EventDialog(this, null, Operation.ADD, true);
    }

    private void copyEventActionPerformed(ActionEvent evt) {
        unArmButton(evt);
        int row = alarmTable.getTable().getSelectedRow();
        if (row == -1) return;
        new EventDialog(this, mainFrame.getEventList().get(row), Operation.ADD, true);
    }

    private void editEventActionPerformed(ActionEvent evt) {
        unArmButton(evt);
        int row = alarmTable.getTable().getSelectedRow();
        if (row == -1) return;
        new EventDialog(this, mainFrame.getEventList().get(row), Operation.MODIFY, true);
    }

    private void deleteEventActionPerformed(ActionEvent evt) {
        unArmButton(evt);
        int row = alarmTable.getTable().getSelectedRow();
        if (row == -1) return;
        int userInput = JOptionPane.showConfirmDialog(this, "Do you want to delete \"" +
                        alarmTable.getTableModel().getValueAt(row, 1) + " - " +
                        alarmTable.getTableModel().getValueAt(row, 3) + "\"?", "ASClock",
                JOptionPane.YES_NO_OPTION,
                JOptionPane.QUESTION_MESSAGE);
        if (userInput == JOptionPane.YES_OPTION) {
            mainFrame.removeEventAndRecount(row);
            alarmTable.getTableModel().removeRow(row);
        }
    }

    public void showAlarmMessage(String sndPath, String message) {
        new AlarmDialog(this, sndPath, message, false);
    }

    // Release pressed state of a button
    private void unArmButton(ActionEvent evt) {
        if (evt.getSource() instanceof JButton) {
            ((JButton) evt.getSource()).getModel().setArmed(false);
        }
    }

    private void setDimensionsAndUnfocus(JButton btn) {
        btn.setFocusable(false);
        //btn.setPreferredSize(new Dimension(90, 50));  //for equal button size in main menu also uncomment this
    }


    private JMenuBar menuBar;
    private JMenu fileMenu;
    private JMenuItem loadM;
    private JMenuItem saveM;
    private JSeparator sep;
    private JMenuItem exitM;

    private JMenu actionsMenu;
    private JMenuItem addM;
    private JMenuItem editM;
    private JMenuItem deleteM;
    private JMenuItem copyM;

    private JMenu optionsMenu;
    private JMenuItem themeM;
    private JMenu themeMenu;
    private JMenuItem lightThemeM;
    private JMenuItem darkThemeM;
    private JCheckBoxMenuItem stayOnTopM;
    private JCheckBoxMenuItem trayM;

    private JMenuItem aboutMenu;
    private JMenuItem aboutM;

    private JFileChooser fileChooser;
    private JButton addB;
    private JButton editB;
    private JButton deleteB;
    private JButton copyB;

    private SystemTray tray;
    private PopupMenu popupM;
    private TrayIcon trayIcon;
    private MenuItem mainT;
    private MenuItem addEventT;
    private MenuItem aboutT;
    private MenuItem exitT;

}


