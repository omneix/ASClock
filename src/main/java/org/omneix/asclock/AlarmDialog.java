package org.omneix.asclock;

import com.formdev.flatlaf.FlatClientProperties;

import javax.swing.*;
import javax.swing.text.SimpleAttributeSet;
import javax.swing.text.StyleConstants;
import javax.swing.text.StyledDocument;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.text.DateFormat;
import java.util.Date;

public class AlarmDialog extends JDialog {

    private WMP3Player mp3Player;
    private String sndPath;
    private String message;
    private static final int DEFAULT_WIDTH = 400;
    private static final int DEFAULT_HEIGHT = 250;

    public AlarmDialog(MainFrameView parent, String sndPath, String message, boolean modal) {
        super(parent, modal);
        this.sndPath = sndPath;
        this.message = message;
        this.mp3Player = parent.getMainFrame().getMp3Player();

        MiscTools.GUIRunAndWait(() -> {
            initComponents();
            setMinimumSize(new Dimension(DEFAULT_WIDTH, DEFAULT_HEIGHT));
            setTitle("ASClock");
            setLocationRelativeTo(null);
            setVisible(true);
            setDefaultCloseOperation(WindowConstants.DISPOSE_ON_CLOSE);
            getRootPane().putClientProperty(FlatClientProperties.MENU_BAR_EMBEDDED, false);
            setResizable(false);
            setAlwaysOnTop(false);
            pack();
        });
    }

    public void initComponents() {
        setLayout(new BoxLayout(getContentPane(), BoxLayout.Y_AXIS));

        timeL = new JLabel();
        setTimeOnLabel();
        timeL.setFont(new javax.swing.plaf.FontUIResource("SEGOE UI", Font.BOLD, 22));
        timeL.setAlignmentX(Component.CENTER_ALIGNMENT);

        textPane = new JTextPane();
        textPane.setContentType("text/html");
        textPane.setText(message);
        StyledDocument doc = textPane.getStyledDocument();
        SimpleAttributeSet attSet = new SimpleAttributeSet();
        StyleConstants.setAlignment(attSet, StyleConstants.ALIGN_CENTER);
        textPane.setFont(new javax.swing.plaf.FontUIResource("SEGOE UI", Font.BOLD, 12));
        if (textPane.getText().length() < 120) {
            int size = StyleConstants.getFontSize(attSet);
            StyleConstants.setFontSize(attSet, size * 2);
        }
        doc.setParagraphAttributes(0, doc.getLength(), attSet, false);
        textPane.setEditable(false);
        textPane.setOpaque(false);

        scrollPane = new JScrollPane(textPane);
        scrollPane.setBorder(BorderFactory.createEmptyBorder());
        scrollPane.setMaximumSize(new Dimension(300, 65));
        scrollPane.setHorizontalScrollBarPolicy(JScrollPane.HORIZONTAL_SCROLLBAR_NEVER);
        scrollPane.setVerticalScrollBarPolicy(JScrollPane.VERTICAL_SCROLLBAR_NEVER);
        scrollPane.setAlignmentX(Component.CENTER_ALIGNMENT);

        okB = new JButton("OK");
        okB.setMaximumSize(new Dimension(100, 50));
        okB.setAlignmentX(Component.CENTER_ALIGNMENT);
        okB.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                stopRepeat();
                dispose();
            }
        });

        add(Box.createRigidArea(new Dimension(0, 15)));
        add(timeL);
        add(Box.createRigidArea(new Dimension(0, 15)));
        add(scrollPane);
        add(Box.createRigidArea(new Dimension(0, 20)));
        add(okB);
        mp3Player.setRepeat(true);
        if (sndPath != null && !sndPath.contains("(None).mp3")) mp3Player.addToPlaylistAndPlay(sndPath);

        this.addWindowListener(new WindowAdapter() {
            @Override
            public void windowClosing(WindowEvent evt) {
                stopRepeat();
                super.windowClosing(evt);
            }
        });
    }

    private void stopRepeat() {
        if (!mp3Player.isStopped()) mp3Player.stopAndClear();
        mp3Player.setRepeat(false);
    }

    public void setTimeOnLabel() {
        Timer t = new Timer(500, e -> timeL.setText(DateFormat.getTimeInstance(2).format(new Date())));
        t.setRepeats(true);
        t.setCoalesce(true);
        t.setInitialDelay(0);
        t.start();
    }

    private JTextPane textPane;
    private JScrollPane scrollPane;
    private JButton okB;
    private JLabel timeL;
}

