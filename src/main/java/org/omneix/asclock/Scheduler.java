package org.omneix.asclock;

import java.time.Duration;
import java.time.LocalTime;
import java.time.temporal.ChronoUnit;
import java.util.HashMap;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

public class Scheduler {

    private final MainFrame mainFrame;
    private final HashMap<LocalTime, Integer> alarmMap;

    public Scheduler(MainFrame mainFrame) {
        this.mainFrame = mainFrame;
        alarmMap = mainFrame.getAlarmMap();
    }

    public void start() {
        LocalTime nextMinute = LocalTime.now().plus(1, ChronoUnit.MINUTES).withSecond(1).withNano(0);
        Duration d = Duration.between(LocalTime.now(), nextMinute);
        long initialDelay = d.getSeconds();

        ScheduledExecutorService scheduler = Executors.newScheduledThreadPool(1);
        scheduler.scheduleAtFixedRate(new Task(),
                initialDelay,
                TimeUnit.MINUTES.toSeconds(1),
                TimeUnit.SECONDS);

    }

    class Task implements Runnable {

        @Override
        public void run() {
            LocalTime now = LocalTime.now().withSecond(0).withNano(0);
            if (alarmMap.containsKey(now)) {
                mainFrame.launchAlarm(alarmMap.get(now));
            }
            if (now.equals(LocalTime.MIDNIGHT)) {
                alarmMap.clear();
                AlarmListHandler.getAllAlarms(mainFrame.getEventList(), alarmMap);
            }
        }
    }

}


