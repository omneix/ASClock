package org.omneix.asclock;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;

class Event implements Comparable<Event> {

    private int number;
    private boolean enabled;
    private boolean cb8Check;
    private final boolean[] scheduledDays = new boolean[]{false, false, false, false, false, false, false, false};
    private String messageAText;
    private String soundsCBText;
    private String time;

    @JsonProperty("scheduled_days")
    public boolean[] getScheduledDays() {
        return scheduledDays;
    }

    @JsonIgnore
    public boolean isEveryday() {
        if (scheduledDays[0]) return true;
        for (boolean day : scheduledDays) {
            if (day) return false;
        }
        return true;
    }

    public void setNumber(int number) {
        this.number = number;
    }

    public int getNumber() {
        return number;
    }

    @JsonProperty("enabled")
    public boolean isEnabled() {
        return enabled;
    }

    public void setEnabled(boolean enabled) {
        this.enabled = enabled;
    }

    @JsonProperty("show_message")
    public boolean isCb8Check() {
        return cb8Check;
    }

    public void setCb8Check(boolean cb8Check) {
        this.cb8Check = cb8Check;
    }

    @JsonProperty("message")
    public String getMessageAText() {
        return messageAText;
    }

    public void setMessageAText(String messageAText) {
        this.messageAText = messageAText;
    }

    @JsonProperty("sound_title")
    public String getSoundsCBText() {
        return soundsCBText;
    }

    public void setSoundsCBText(String soundsCBText) {
        this.soundsCBText = soundsCBText;
    }

    @JsonProperty("time")
    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    @Override
    public int compareTo(Event o) {
        return Integer.compare(this.getTime().compareTo(o.getTime()), 0);
    }
}
