package org.omneix.asclock;

import com.formdev.flatlaf.FlatClientProperties;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class AboutDialog extends JDialog {

    public AboutDialog(MainFrameView parent, boolean modal) {
        super(parent, modal);

        MiscTools.GUIRunAndWait(() -> {
            initComponents();
            setTitle("About ASClock");
            setLocationRelativeTo(null);
            setVisible(true);
            setDefaultCloseOperation(WindowConstants.DISPOSE_ON_CLOSE);
            getRootPane().putClientProperty(FlatClientProperties.MENU_BAR_EMBEDDED, false);
            setResizable(false);
            setAlwaysOnTop(false);
            pack();
        });
    }

    public void initComponents() {
        titleL = new JLabel("Alarm Simple Clock");
        titleL.setFont(new Font(getName(), Font.BOLD, 15));

        textL = new JLabel("Made by omneix.");
        textL.setMinimumSize(new Dimension(50,60));

        picLabel = new JLabel(new ImageIcon(Toolkit.getDefaultToolkit().getImage(getClass()
                .getResource("/images/clock.png")).getScaledInstance(50, 50, java.awt.Image.SCALE_SMOOTH)));

        closeB = new JButton("Close");
        closeB.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                dispose();
            }
        });

        picLabel.setMaximumSize(new Dimension(50,50));
        picLabel.setMinimumSize(new Dimension(50, 50));
        picLabel.setSize(new Dimension(50,50));

        GroupLayout layout = new GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
                layout.createParallelGroup(GroupLayout.Alignment.LEADING)
                        .addGroup(layout.createSequentialGroup()
                                .addGap(10)
                                .addGroup(layout.createParallelGroup(GroupLayout.Alignment.LEADING)
                                        .addGroup(layout.createSequentialGroup()
                                                .addComponent(picLabel)
                                                .addGap(10)
                                                .addGroup(layout.createParallelGroup()
                                                        .addComponent(titleL)
                                                        .addComponent(textL)))
                                        .addGroup(layout.createSequentialGroup()
                                                .addGap(124)
                                                .addComponent(closeB)))
                                .addGap(10)));
        layout.setVerticalGroup(
                layout.createParallelGroup(GroupLayout.Alignment.LEADING)
                        .addGroup(layout.createSequentialGroup()
                                .addGap(10)
                                .addGroup(layout.createParallelGroup(GroupLayout.Alignment.LEADING)
                                        .addComponent(picLabel)
                                        .addGroup(layout.createParallelGroup(GroupLayout.Alignment.LEADING)
                                            .addComponent(titleL)
                                            .addComponent(textL)))
                                .addComponent(closeB)
                                .addGap(10)));
        pack();
    }

    private JLabel picLabel;
    private JLabel titleL;
    private JLabel textL;
    private JButton closeB;
}
