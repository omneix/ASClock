package org.omneix.asclock;

import java.io.File;
import java.net.URISyntaxException;
import java.time.LocalTime;
import java.util.*;
import java.util.List;
import java.util.stream.IntStream;

import static org.omneix.asclock.MiscTools.*;
import static java.awt.EventQueue.invokeLater;

public class MainFrame {

    private final MainFrameView view;
    private List<Event> eventList;
    private String[] soundList;
    private final HashMap<LocalTime, Integer> alarmMap = new HashMap<>();
    private final WMP3Player mp3Player = new WMP3Player();
    private String soundDirPath;

    public MainFrame() {
        eventList = EventPropertiesManager.loadEventsFromFile();
        AlarmListHandler.getAllAlarms(eventList, alarmMap);
        Scheduler scheduler = new Scheduler(this);
        scheduler.start();
        fillSoundList();
        view = new MainFrameView(this);
    }

    public static void main(String[] args) {
        PrefManager.loadPreferences();
        setLookAndFeel(PrefManager.getTheme());

        invokeLater(() -> {
            final MainFrame mainFrame = new MainFrame();
            mainFrame.getView().setVisible(true);
        });
    }

    public MainFrameView getView() {
        return view;
    }

    public String[] getSoundList() {
        return soundList;
    }

    public List<Event> getEventList() {
        return eventList;
    }

    public HashMap<LocalTime, Integer> getAlarmMap() {
        return alarmMap;
    }

    public WMP3Player getMp3Player() {
        return mp3Player;
    }

    public void setEventList(List<Event> eventList) {
        this.eventList = eventList;
    }

    public String getSoundDirPath() {
        return soundDirPath;
    }

    private void fillSoundList() {
        setSoundDirPath();
        if (soundDirPath != null) {
            File[] sounds = new File(soundDirPath).listFiles();
            soundList = new String[sounds.length + 1];
            soundList[0] = "(None)";
            for (int i = 1; i < soundList.length; i++) {
                soundList[i] = sounds[i - 1].getName().split("\\.")[0];
            }
        } else {
            soundList = new String[]{("(None)")};
        }
    }

    private void setSoundDirPath() {
        String url;
        try {
            url = new File(MainFrame.class.getProtectionDomain().getCodeSource().getLocation()
                    .toURI()).getParentFile() + "/Sound";
        } catch (URISyntaxException e) {
            throw new RuntimeException(e);
        }
        File file = new File(url);
        if (file.exists() && file.isDirectory()) {
            soundDirPath = file.getPath() + "\\";
        } else {
            soundDirPath = null;
        }
    }

    public void removeEventAndRecount(int eventRow) {
        eventList.remove(eventRow);
        IntStream.range(0, eventList.size())
                .forEach(i -> eventList.get(i).setNumber(i));
    }

    public void performRearrange() {
        eventList.sort(Comparator.naturalOrder());
        recountEventIndexes();
        view.getAlarmTable().getTableModel().refillTableFromList(eventList);
        AlarmListHandler.getAllAlarms(eventList, alarmMap);
    }

    private void recountEventIndexes() {
        IntStream.range(0, eventList.size())
                .forEach(i -> eventList.get(i).setNumber(i));
    }

    public void launchAlarm(int eventNumber) {
        String fullSoundPath = eventList.get(eventNumber).getSoundsCBText();
        if (!fullSoundPath.matches(".*\\.mp3") && soundDirPath != null) {
            fullSoundPath = soundDirPath + fullSoundPath + ".mp3";
        } else if (!fullSoundPath.contains(".mp3")) {
            fullSoundPath = null;
        }
        view.showAlarmMessage(fullSoundPath, eventList.get(eventNumber).getMessageAText());
    }

}
